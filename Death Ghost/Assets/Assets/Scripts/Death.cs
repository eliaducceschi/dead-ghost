﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {

    private static int ActualDeaths = 0;

    [SerializeField]
    private GameObject m_PlayersParent;

    [SerializeField]
    private int m_MaxDeaths;

    private List<GameObject> m_Players;

    private void Awake()
    {
        ActualDeaths = m_MaxDeaths;
        m_Players = new List<GameObject>();
        foreach (Transform child in m_PlayersParent.transform)
        {
            m_Players.Add(child.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (GameObject player in m_Players)
        { 
            if (collision.gameObject == player)
            {
                ActualDeaths--;
                if(ActualDeaths == 0)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }else
                {
                    player.SetActive(false);
                }
            }
        }
    }

}
