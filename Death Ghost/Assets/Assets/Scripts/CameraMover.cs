﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    [SerializeField]
    private float m_VerticalSpeed;

    [SerializeField]
    private float m_ZSpeed;

    private int m_VerticalDirection;
    private int m_ZDirection;

    private void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow))
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                m_VerticalDirection = -1;
            }
            else
            {
                m_VerticalDirection = 1;
            }
        }
        else
        {
            m_VerticalDirection = 0;
        }

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                m_ZDirection = -1;
            }
            else
            {
                m_ZDirection = 1;
            }
        }
        else
        {
            m_ZDirection = 0;
        }

        MoveCamera();
    }

    private void MoveCamera()
    {
        float spostamentoVerticale = m_VerticalSpeed * Time.deltaTime;
        float spostamentoZ = m_ZSpeed * Time.deltaTime;

        spostamentoVerticale *= m_VerticalDirection;
        spostamentoZ = m_ZDirection;

        gameObject.transform.position += new Vector3(0, spostamentoVerticale, spostamentoZ);
    }
}
