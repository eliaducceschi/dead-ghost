﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneStarter : MonoBehaviour {

    [SerializeField]
    private KeyCode m_StartKey;

    [SerializeField]
    private string m_SceneName;
    
    void Update () {
        if (Input.GetKeyDown(m_StartKey))
        {
            SceneManager.LoadScene(m_SceneName);
        }
    }
}
