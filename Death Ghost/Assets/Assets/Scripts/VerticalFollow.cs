﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalFollow : MonoBehaviour {

    [SerializeField]
    private GameObject m_ToFollow;

    private void Update()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x,
            m_ToFollow.transform.position.y,
            gameObject.transform.position.z);
    }
}
