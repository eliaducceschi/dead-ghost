﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

    [SerializeField]
    private int m_SphereNeeded;

    private int m_Absorbed;

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.SetActive(false);
        m_Absorbed++;
        if(m_SphereNeeded <= m_Absorbed)
        {
            Win();
        }
    }
    private void Win()
    {
        SceneManager.LoadScene("Win");
    }
}
