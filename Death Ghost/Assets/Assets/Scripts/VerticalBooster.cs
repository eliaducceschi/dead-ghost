﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalBooster : MonoBehaviour {
    
    private Rigidbody m_Boosted;

    [SerializeField]
    private float m_ForcePerSecond;

    [SerializeField]
    private bool m_Vertical;

    [SerializeField]
    private KeyCode m_UpKey;

    [SerializeField]
    private KeyCode m_DownKey;

    private void Update()
    {
        m_Boosted = gameObject.GetComponent<Rigidbody>();
        m_Boosted.centerOfMass = new Vector3(0, 0, 0);
    }

    private void FixedUpdate()
    {
        if (m_Vertical)
        {
            if (Input.GetKey(m_UpKey))
            {
                m_Boosted.AddForce(new Vector3(0, m_ForcePerSecond, 0) * Time.fixedDeltaTime);
            }
            else if (Input.GetKey(m_DownKey))
            {
                m_Boosted.AddForce(new Vector3(0, -m_ForcePerSecond, 0) * Time.fixedDeltaTime);
            }
        }
        else
        {
            if (Input.GetKey(m_UpKey))
            {
                m_Boosted.AddForce(new Vector3(m_ForcePerSecond, 0, 0) * Time.fixedDeltaTime);
            }
            else if (Input.GetKey(m_DownKey))
            {
                m_Boosted.AddForce(new Vector3(-m_ForcePerSecond, 0, 0) * Time.fixedDeltaTime);
            }
        }
    }


}
