﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointEraser : MonoBehaviour
{ 
    [SerializeField]
    private Joint m_ToErase;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && m_ToErase != null)
        {
            Destroy(m_ToErase);
            m_ToErase = null;
        }
    }

}
