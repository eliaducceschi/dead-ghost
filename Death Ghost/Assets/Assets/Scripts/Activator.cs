﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Activator : MonoBehaviour {

    [SerializeField]
    private GameObject m_TriggersParent;

    private List<GameObject> m_Triggers;

    [SerializeField]
    private MonoBehaviour[] m_Activators;

    [SerializeField]
    private Material m_WhenActive;
    [SerializeField]
    private Material m_WhenDisabled;

    private bool m_Active = false;
    private int m_ActualTriggers = 0;

    private void Awake()
    {
        m_Triggers = new List<GameObject>();
        foreach(Transform child in m_TriggersParent.transform)
        {
            m_Triggers.Add(child.gameObject);
        }

        UpdateActivatorState();
    }

    private void FixedUpdate()
    {
        if (m_Active)
        {
            UpdateActivatorState();
            m_Active = false;
        }
        else
        {
            UpdateActivatorState();
        }
    }

    private void OnTriggerStay(Collider i_Other)
    {
        foreach (GameObject trigger in m_Triggers)
        {
            if (i_Other.gameObject == trigger)
            {
                m_Active = true;
            }
        }
    }

    private void UpdateActivatorState()
    {
        foreach (MonoBehaviour activator in m_Activators)
        {
            if(activator != null)
            {
                activator.enabled = m_Active;
            }
        }
        UpdateMaterial();
    }

    private void UpdateMaterial()
    {
        Material toApply = m_WhenDisabled;
        if (m_Active)
        {
            toApply = m_WhenActive;
        }
        RecursiveUpdateMaterial(gameObject.transform, toApply);
    }

    private void RecursiveUpdateMaterial(Transform i_Parent, Material i_ToApply)
    {
        Renderer renderer = i_Parent.gameObject.GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.material = i_ToApply;
        }

        foreach (Transform child in i_Parent)
        {
            RecursiveUpdateMaterial(child, i_ToApply);
        }
    }

}
