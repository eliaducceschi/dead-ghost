﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringJointLoader : MonoBehaviour {

    [SerializeField]
    private GameObject m_SpringJoint;

    [SerializeField]
    private bool m_LoadTopBottom = true;

    [SerializeField]
    private float m_MaxLoad;

    [SerializeField]
    private float m_LoadVelocity;

    [SerializeField]
    private KeyCode m_LoadKey;

    [SerializeField]
    private KeyCode m_ReleaseKey;

    private GameObject m_CopiedSpring = null;
    private float m_Load = 0;

    private bool m_IsLoading;

    private void Awake()
    {
        m_MaxLoad = m_MaxLoad * gameObject.transform.localScale.x;
    }

    private void Update()
    {
        CheckSpacePressed();

        if(m_CopiedSpring != null)
        {
            if (m_IsLoading)
            {
                LoadSpring();
            }
            else
            {
                UnloadSpring();
            }
        }

    }

    private void LoadSpring()
    {
        if (m_Load < m_MaxLoad)
        {
            MoveSpring(true);
        }

    }

    private void UnloadSpring()
    {
        if (m_Load > 0)
        {
            MoveSpring(false);
        }
        else
        {
            Release();
        }

    }

    private void CheckSpacePressed()
    {
        if (Input.GetKeyDown(m_LoadKey))
        {
            if(m_CopiedSpring == null)
            {
                m_CopiedSpring = GameObject.Instantiate(m_SpringJoint, m_SpringJoint.transform.parent);
            }
            m_IsLoading = true;

        }
        else if (Input.GetKeyUp(m_LoadKey))
        {
            m_IsLoading = false;
        }

        if (Input.GetKeyDown(m_ReleaseKey))
        {
            Release();
        }
    }

    private void MoveSpring(bool load)
    {
        float spostamento = m_LoadVelocity * Time.deltaTime;

        if (load)
        {
            m_Load += spostamento;
            if (m_LoadTopBottom)
            {
                spostamento *= -1;
            }
            m_CopiedSpring.transform.localPosition += new Vector3(0, spostamento, 0);
        }
        else
        {
            m_Load -= spostamento;
            if (m_LoadTopBottom)
            {
                spostamento *= -1;
            }
            m_CopiedSpring.transform.localPosition -= new Vector3(0, spostamento, 0);
        }

    }

    private void Release()
    {
        Destroy(m_CopiedSpring);
        m_CopiedSpring = null;
        m_Load = 0;
        m_IsLoading = true;
    }

    private void OnDisable()
    {
        Release();
    }
}
