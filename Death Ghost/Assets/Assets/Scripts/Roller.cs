﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roller : MonoBehaviour
{

    private Rigidbody m_Roller;

    [SerializeField]
    private float m_ForcePerSecond;

    [SerializeField]
    private KeyCode m_ClockwiseKey;

    [SerializeField]
    private KeyCode m_CounterclockwiseKey;

    private void Awake()
    {
        m_Roller = gameObject.GetComponent<Rigidbody>();
        m_Roller.centerOfMass = new Vector3(0, 0, 0);
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(m_ClockwiseKey))
        {
            m_Roller.AddTorque(new Vector3(0, 0, -m_ForcePerSecond) * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
        else if (Input.GetKey(m_CounterclockwiseKey))
        {
            m_Roller.AddTorque(new Vector3(0, 0, m_ForcePerSecond) * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }


}
