﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapisRoulant : MonoBehaviour {

    [SerializeField]
    private float m_ForsePerSecond;

    private List<Rigidbody> m_ToAccelerate;

    private void Awake()
    {
        m_ToAccelerate = new List<Rigidbody>();
    }

    private void OnCollisionStay(Collision collision)
    {
        Rigidbody rigidbody = collision.gameObject.GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            m_ToAccelerate.Add(rigidbody);
        }
        
    }

    private void FixedUpdate()
    {
        foreach(Rigidbody rigidbody in m_ToAccelerate)
        {
            rigidbody.AddForce(new Vector3(m_ForsePerSecond * Time.fixedDeltaTime, 0, 0));
        }
        m_ToAccelerate.Clear();
    }
}
